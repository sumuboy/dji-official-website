// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: {enabled: true},
  pages: true,
  app: {
    head: {
      script: [
        {src: '/_nuxt/assets/icon/iconfont.js'}
      ]
    }
  }
})
