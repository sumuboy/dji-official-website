/**
 * 防抖装饰器
 * @param func 执行函数
 * @param ms  防抖时间(毫秒)
 */
export const debounce = (func: Function, ms: number) => {
  let time: any
  return function () {
    clearTimeout(time)
    // @ts-ignore
    time = setTimeout(() => func.apply(this, arguments), ms)
  }
}

/**
 * 节流装饰器
 * @param func 执行函数
 * @param ms  节流时间(毫秒)
 */
export const throttle = (func: Function, ms: number) => {
  let isThrottled = false

  return function () {
    if (!isThrottled) {
      // @ts-ignore
      func.apply(this, arguments)
      isThrottled = true
      setTimeout(() => isThrottled = false, ms)
    }
  }
}