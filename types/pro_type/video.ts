export type videoItem = {
  id: number,
  url: string,
  poster: string
}

export type videoPlace = {
  id: number,
  name: string,
  btn: string[]
}