export interface HeaderPropsType {
  fixed?: boolean,
  backgroundColor?: string
}