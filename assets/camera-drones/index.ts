import djiMavic from "./3b7953435e791d6a56253d87584e34cf.png"
import djiAir from "./77738ff32f7a228fc58b4f401261e481.png"
import djiMini from "./3b2958e69dfc377784a5a995c8c16c06.png"
import djiAvata from "./0937a9739b844ce6d58ea6363d67b259.png"
import djiInspire from "./d13ba498aecd93533f69bcd1500458af.png"
import djiffpv from "./445acaa272ac55184d736f9518c50b3c.png"
import rc from "./b429552c65fa90e612204031c752b2e0.png"
import djipower from "./55a6b0d43b462306c6b0c92ca0f764cc.png"
import helpme from "./dd80d4f36df9091f5ebcebd5fe864399.svg"
import duibi from "./dbb0d774602c8769b173939c66bf4bfa.svg"
import xduibi from "./dbb0d774602c8769b173939c66bf4bfa.svg"
import peijian from "./42af7c7d7cc7b425c30b5ec52859318c.svg"
import liji from "./a46f9b5e0f5b88cb1e1e4a9ae10a42bb.svg"
import contextBg from "./e6718e63aca0484a8e1e7db68eed09f8@origin.jpg"

import card1 from "~/assets/camera-drones/c2cf5f3b8c738ee773a4035a4da36214.jpg";
import card2 from "~/assets/camera-drones/c52c9d17f4d1b3695be28dc5a905c7af.jpg"
import card3 from "~/assets/camera-drones/e49a66fff20c5b41a43665582a770578.jpg"
import card4 from "~/assets/camera-drones/c5cd0f205aec511abb3cff468806dbe2.jpg"


export {djiMavic, djiAir, djiMini, djiAvata, djiInspire, djiffpv, rc, djipower, helpme, duibi, xduibi, peijian, liji, contextBg, card1, card2, card3, card4}