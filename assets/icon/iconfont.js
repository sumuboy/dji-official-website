if (window) {
  window._iconfont_svg_string_4502446 = '<svg><symbol id="icon-jiantou" viewBox="0 0 1024 1024"><path d="M341.957209 959.354678c-12.893658 0-25.842575-4.425799-36.386722-13.467733-23.493064-20.10592-26.224268-55.426357-6.064113-78.891791L603.929736 511.789199l-304.423361-355.206978c-20.160155-23.465434-17.427928-58.786894 6.064113-78.891791 23.437805-20.024056 58.786894-17.373693 78.837556 6.064113l335.675132 391.620305c17.974373 20.952194 17.974373 51.875484 0 72.827679l-335.675132 391.620305C373.371686 952.717514 357.691565 959.354678 341.957209 959.354678z"  ></path></symbol><symbol id="icon-fenxiang" viewBox="0 0 1024 1024"><path d="M128 213.333333a85.333333 85.333333 0 0 1 85.333333-85.333333h298.666667a42.666667 42.666667 0 1 1 0 85.333333H213.333333v597.333334h597.333334v-298.666667a42.666667 42.666667 0 1 1 85.333333 0v298.666667a85.333333 85.333333 0 0 1-85.333333 85.333333H213.333333a85.333333 85.333333 0 0 1-85.333333-85.333333V213.333333zM853.333333 128a42.666667 42.666667 0 0 1 42.666667 42.666667v213.333333a42.666667 42.666667 0 1 1-85.333333 0V170.666667a42.666667 42.666667 0 0 1 42.666666-42.666667z"  ></path><path d="M896 170.666667a42.666667 42.666667 0 0 1-42.666667 42.666666h-213.333333a42.666667 42.666667 0 1 1 0-85.333333h213.333333a42.666667 42.666667 0 0 1 42.666667 42.666667z"  ></path><path d="M844.501333 179.498667a42.666667 42.666667 0 0 1 0 60.330666l-281.002666 281.002667a42.666667 42.666667 0 0 1-60.330667-60.330667l281.002667-281.002666a42.666667 42.666667 0 0 1 60.330666 0z"  ></path></symbol></svg>', function (n) {
    var t = (t = document.getElementsByTagName("script"))[t.length - 1], e = t.getAttribute("data-injectcss"), t = t.getAttribute("data-disable-injectsvg");
    if (!t) {
      var o, i, a, d, c, s = function (t, e) {
        e.parentNode.insertBefore(t, e)
      };
      if (e && !n.__iconfont__svg__cssinject__) {
        n.__iconfont__svg__cssinject__ = !0;
        try {
          document.write("<style>.svgfont {display: inline-block;width: 1em;height: 1em;fill: currentColor;vertical-align: -0.1em;font-size:16px;}</style>")
        } catch (t) {
          console && console.log(t)
        }
      }
      o = function () {
        var t, e = document.createElement("div");
        e.innerHTML = n._iconfont_svg_string_4502446, (e = e.getElementsByTagName("svg")[0]) && (e.setAttribute("aria-hidden", "true"), e.style.position = "absolute", e.style.width = 0, e.style.height = 0, e.style.overflow = "hidden", e = e, (t = document.body).firstChild ? s(e, t.firstChild) : t.appendChild(e))
      }, document.addEventListener ? ~["complete", "loaded", "interactive"].indexOf(document.readyState) ? setTimeout(o, 0) : (i = function () {
        document.removeEventListener("DOMContentLoaded", i, !1), o()
      }, document.addEventListener("DOMContentLoaded", i, !1)) : document.attachEvent && (a = o, d = n.document, c = !1, r(), d.onreadystatechange = function () {
        "complete" == d.readyState && (d.onreadystatechange = null, l())
      })
    }

    function l() {
      c || (c = !0, a())
    }

    function r() {
      try {
        d.documentElement.doScroll("left")
      } catch (t) {
        return void setTimeout(r, 50)
      }
      l()
    }
  }(window);
}